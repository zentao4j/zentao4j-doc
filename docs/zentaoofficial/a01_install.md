## 安装前准备

### 安装 docker
略.

### 下载 zentao 源码包
下载地址: [https://www.zentao.net/page/download.html](https://www.zentao.net/page/download.html)
![](/assets/image/zentaoofficial/install/001.png)

## docker 搭建 dnmp 环境
### docker 目录结构
```shell
.
├── docker-compose.yml
├── docker.env
├── mysql
│   └── standalone
│       ├── conf
│       │   └── my.cnf
│       ├── data
│       └── log
└── nginx
    ├── conf
    │   ├── conf.d
    │   └── nginx.conf
    ├── html
    │   └── phpinfo.php
    └── log

10 directories, 5 files
laolang@laolang-pc:zentao$ 
```

!!! note "注意"
    `mysql/standalone/log` 权限为 777
    

### nginx 配置文件
```nginx linenums="1"
#允许进程数量，建议设置为cpu核心数或者auto自动检测，注意Windows服务器上虽然可以启动多个processes，但是实际只会用其中一个
worker_processes  1; 

events {
    #单个进程最大连接数（最大连接数=连接数*进程数）
    #根据硬件调整，和前面工作进程配合起来用，尽量大，但是别把cpu跑到100%就行。
    worker_connections  1024;
}

http {
    #文件扩展名与文件类型映射表(是conf目录下的一个文件)
    include       mime.types;
    #默认文件类型，如果mime.types预先定义的类型没匹配上，默认使用二进制流的方式传输
    default_type  application/octet-stream;

    #sendfile指令指定nginx是否调用sendfile 函数（zero copy 方式）来输出文件，对于普通应用，必须设为on。如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络IO处理速度。
    sendfile        on;
    
     #长连接超时时间，单位是秒
    keepalive_timeout  65;

    # spring boot 后端
    server {
        listen 80;
        server_name www.zentaolocal.com;
        location / {
            root /usr/share/nginx/html;
            index index.html index.php;
        }

        location ~ \.php$ {
            root /usr/share/nginx/html;
            fastcgi_pass    zentao-php:9000;
            fastcgi_index   index.php;
            fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
			fastcgi_param   SCRIPT_NAME      $fastcgi_script_name;
			include         fastcgi_params;
        }
    }
}
```

### mysql 配置文件
``` linenums="1"
[mysqld]
user                    = mysql
port                    = 3306
pid-file                = /var/run/mysqld/mysqld.pid
socket                  = /var/run/mysqld/mysqld.sock
lc-messages-dir = /usr/share/mysql

basedir                 = /usr
datadir                 = /var/lib/mysql
tmpdir                  = /tmp

server-id       = 1 
log-bin         = mysql-bin 

default-storage-engine  = INNODB
character-set-server    = utf8
collation-server                = utf8_general_ci

#bind-address   = 127.0.0.1
log-error  = /var/log/mysql/error.log

lower_case_table_names  = 1
explicit_defaults_for_timestamp = true

symbolic-links  = 0
sql_mode                = NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES

slow_query_log = ON
slow_query_log_file = /var/log/mysql/slow.log
long_query_time = 1

[client]
port        = 3306
socket      = /var/run/mysqld/mysqld.sock
default-character-set=utf8

[mysqld_safe]
pid-file    = /var/run/mysqld/mysqld.pid
socket      = /var/run/mysqld/mysqld.sock
nice        = 0
```

### docker-compose.yml

=== "docker-compose.yml"

    ```yml linenums="1"
    version: '3'
    services:
    # mysql
    zentao-mysql:
        image: mysql:5.7
        privileged: true
        container_name: zentao-mysql
        ports:
        - "12301:3306"
        volumes:
        - ${ZENTAO_BASE_DIR}/mysql/standalone/conf/my.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf
        - ${ZENTAO_BASE_DIR}/mysql/standalone/data:/var/lib/mysql
        - ${ZENTAO_BASE_DIR}/mysql/standalone/log:/var/log/mysql
        networks:
        zentao:
            ipv4_address: ${ZENTAO_NETWORK_IP_PREFIX}.0.2
        environment:
        MYSQL_ROOT_PASSWORD: root

    # php
    zentao-php:
        image: bitnami/php-fpm:7.4
        container_name: zentao-php
        volumes:
        - ${ZENTAO_BASE_DIR}/nginx/html:/usr/share/nginx/html
        networks:
        zentao:
            ipv4_address: ${ZENTAO_NETWORK_IP_PREFIX}.0.3  
        depends_on:
        - zentao-mysql

    # nginx , 反向代理后端及 minio 等
    zentao-nginx:
        image: nginx:1.18.0
        container_name: zentao-nginx
        ports:
        - "12303:80"
        volumes:
        - ${ZENTAO_BASE_DIR}/nginx/conf/nginx.conf:/etc/nginx/nginx.conf
        - ${ZENTAO_BASE_DIR}/nginx/conf/conf.d:/etc/nginx/conf.d
        - ${ZENTAO_BASE_DIR}/nginx/log:/var/log/nginx
        - ${ZENTAO_BASE_DIR}/nginx/html:/usr/share/nginx/html
        networks:
        zentao:
            ipv4_address: ${ZENTAO_NETWORK_IP_PREFIX}.0.4
        extra_hosts:
        - "host.docker.internal:host-gateway"
        depends_on:
        - zentao-php
    networks:
    zentao:
        driver: bridge
        ipam:
        driver: default
        config:
            - subnet: ${ZENTAO_NETWORK_IP_PREFIX}.0.0/16
            gateway: ${ZENTAO_NETWORK_IP_PREFIX}.0.1
    ```

=== "docker.env"

    ```env linenums="1"
    # docker 容器相关跟目录
    ZENTAO_BASE_DIR=/home/laolang/program/docker/zentao4j/zentao
    # docker 自定义网络前缀
    ZENTAO_NETWORK_IP_PREFIX=172.30
    ```

### 修改 hosts 文件
```
172.30.0.2 ZENTAO_MYSQL_HOST
172.30.0.4 www.zentaolocal.com
```

### 启动 docker 服务
```shell
docker-compose --env-file=docker.env up -d
```

### 测试 php 环境
添加文件 `nginx/html/phpinfo.php`
```php
<?php

phpinfo();

?>
```

然后访问: [http://www.zentaolocal.com/phpinfo.php](http://www.zentaolocal.com/phpinfo.php)
![](/assets/image/zentaoofficial/install/002.png)

## 禅道安装

### 解压
将下载的禅道源码包复制到 `nginx/html`中并解压
```shell
unzip -q ZenTaoPMS-18.10-php7.2_7.4.zip
```

### 修改 nginx.conf
```conf linenums="1" hl_lines="27 32"
#允许进程数量，建议设置为cpu核心数或者auto自动检测，注意Windows服务器上虽然可以启动多个processes，但是实际只会用其中一个
worker_processes  1; 

events {
    #单个进程最大连接数（最大连接数=连接数*进程数）
    #根据硬件调整，和前面工作进程配合起来用，尽量大，但是别把cpu跑到100%就行。
    worker_connections  1024;
}

http {
    #文件扩展名与文件类型映射表(是conf目录下的一个文件)
    include       mime.types;
    #默认文件类型，如果mime.types预先定义的类型没匹配上，默认使用二进制流的方式传输
    default_type  application/octet-stream;

    #sendfile指令指定nginx是否调用sendfile 函数（zero copy 方式）来输出文件，对于普通应用，必须设为on。如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络IO处理速度。
    sendfile        on;
    
     #长连接超时时间，单位是秒
    keepalive_timeout  65;

    # spring boot 后端
    server {
        listen 80;
        server_name www.zentaolocal.com;
        location / {
            root /usr/share/nginx/html/zentaopms/www;
            index index.html index.php;
        }

        location ~ \.php$ {
            root /usr/share/nginx/html/zentaopms/www;
            fastcgi_pass    zentao-php:9000;
            fastcgi_index   index.php;
            fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
			fastcgi_param   SCRIPT_NAME      $fastcgi_script_name;
			include         fastcgi_params;
        }
    }
}
```

### 重启 nginx 并设置权限
```shell
# 重启 nginx
docker-compose --env-file=docker.env restart zentao-nginx
# 设置 nginx 目录权限,防止出现 403 错误
docker-compose --env-file=docker.env exec zentao-nginx chmod -R 755 /usr/share/nginx/html
# 修改 zentao 相关目录权限
docker-compose --env-file=docker.env exec zentao-nginx chmod -R 777 /usr/share/nginx/html/zentaopms/www/data
docker-compose --env-file=docker.env exec zentao-nginx chmod -R 777 /usr/share/nginx/html/zentaopms/tmp
```

### 开始安装禅道
访问: [http://www.zentaolocal.com](http://www.zentaolocal.com)
#### 系统检查
![](/assets/image/zentaoofficial/install/003.png)

#### 数据库配置
![](/assets/image/zentaoofficial/install/004.png)

#### 保存 my.php
!!! note "注意"
    此文件必须保存,否则无法进行下一步

![](/assets/image/zentaoofficial/install/005.png)

#### 设置管理员账号密码
![](/assets/image/zentaoofficial/install/006.png)

#### 删除文件
![](/assets/image/zentaoofficial/install/007.png)

!!! note "注意"
    虽然提示的是删除 `install.php`,但实际上还需要删除 `upgrade.php`,这两个文件添加 `.txt` 后缀即可
#### 登录禅道
![](/assets/image/zentaoofficial/install/008.png)
![](/assets/image/zentaoofficial/install/009.png)